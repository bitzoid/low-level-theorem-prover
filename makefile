include makefile.config

.PHONY:all tools tests test clean

all: $(BINARYS)
	@echo "All targets built."

tools: $(TOOL_BINARYS)
tests: $(TEST_BINARYS)

$(TOOL_BINARYS): $(BIN)%: $(BUILD)$(TOOLDIR)%.o $(LIBR_OBJECTS)
	@mkdir -p '$(BIN)' && \
	echo '[$(MODE)] Linking tool <$(@:$(TESTBIN)%=%)>.' && \
	$(CXX) $(LDFLAGS) $^ $(LDLIBS) -o '$@' 2>&1

$(TEST_BINARYS): $(TESTBIN)%: $(BUILD)$(TESTDIR)%.o $(LIBR_OBJECTS)
	@mkdir -p '$(TESTBIN)' && \
	echo '[$(MODE)] Linking test <$(@:$(BIN)%=%)>.' && \
	$(CXX) $(LDFLAGS) $^ $(LDLIBS) -o '$@' 2>&1

$(BUILDDIRS): %: makefile makefile.config
	@mkdir -p '$@'

$(OBJECTS): %.o: $(BUILDDIRS) makefile makefile.config
	@echo '[$(MODE)] Compiling <$(@:$(BUILD)%=%)>.' && \
	$(CXX) -MMD -MP -c $(CXXFLAGS) $(patsubst $(BUILD)%.o,%.$(CXXEXT),$@) -o '$@' 2>&1

-include $(DEPENDS)

TESTTARGETS:=$(patsubst %,run_%,$(TESTS))

test:$(TESTTARGETS)
.PHONY:$(TESTTARGETS)

$(TESTTARGETS): run_%: $(TESTBIN)%
	@#echo 'Running test "$(patsubst run_%,%,$@)".'
	@[$(MODE)] /usr/bin/time --quiet -f 'Test "$(patsubst run_%,%,$@)" finished in %Us (prss: %M kB). S:%x.' '$<' 2>&1 | sed 's/S:0/Success/g; s/S:1/FAILURE/g'


.PHONY:clean
CLEANFILES := $(wildcard $(BINARYS) $(BUILD))
clean:
	@touch .dummy.$$$$.tmp && \
	echo "Cleaning files: $(CLEANFILES)" && \
	rm -r $(CLEANFILES) .dummy.$$$$.tmp && \
	echo "Done."
