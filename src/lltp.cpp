#include <iostream>

#include <vector>
#include <string>

#include "core/search.h"

#include "adt/parser.h"

namespace {
  inline void print(std::vector<lltp::Transition> const& trs, std::ostream& os = std::cout) {
    if (trs.empty()) {
      os << "Unable to prove theorem.\n";
    } else {
      bool firstline = true;
      for (auto [f,tag,t]: trs) {
        if (firstline) {
          os << f << "\n";
        }
        firstline = false;
        os << "->\t" << print(tag) << "\t" << t << "\n";
      }
    }
  }
  inline void display(lltp::StepInfo const& si) {
    //if (si.searched % 100) return;
    std::cout << "[Q" << si.id << ": " << si.searched << " / " << si.remain << "]\t";
    std::cout << "Investigated [" << si.e.size() << "]\t" << si.e << "\t(" << si.children_new << " / " << si.children_total << " children)";
    std::cout << "\n";
  }
  struct Args {
    std::optional<std::string> error{};
    std::array<lltp::Expr,2> e{lltp::Expr('?'),lltp::Expr('?')};
    std::unique_ptr<lltp::Expander> expander{};

    Args(std::vector<std::string> const& args) {
      if (args.size() < 2) {
        error = "Expect at least two arguments.";
        return;
      }
      {
        std::string theory = "field";
        if (args.size() > 2) {
          theory = args[0];
        }
        auto make = [](auto t) {
          return std::make_unique<lltp::AlgebraExpander<decltype(t)>>();
        };
        using lltp::EType;
        using namespace lltp::algebra;
        if (false) {
        } else if (theory == "magma") {
          expander = make(Magma<EType::add>());
        } else if (theory == "semigroup") {
          expander = make(Semigroup<EType::add>());
        } else if (theory == "monoid") {
          expander = make(Monoid<EType::add>());
        } else if (theory == "group") {
          expander = make(Group<EType::add>());
        } else if (theory == "abelian" || theory == "abelian_group") {
          expander = make(AbelianGroup<EType::add>());
        } else if (theory == "ring") {
          expander = make(Ring<>());
        } else if (theory == "field") {
          expander = make(Field<>());
        } else if (theory == "legacy_field") {
          expander = std::make_unique<lltp::LegacyFieldExpander>();
        } else {
          error = "Unknown theory '" + theory + "'.";
          return;
        }
      }
      lltp::ExprParser const ep;
      for (std::size_t i = 0; i < 2; ++i) {
        auto const& str = args[(args.size()-2)+i];
        std::optional<lltp::Expr> const opt = ep(str);
        if (!opt) {
          error = "Invalid expression '" + str + "'.";
          return;
        }
        e[i] = *opt;
      }
    }
  };
}

int main(int const argc, char const* const* const argv) {
  Args args(std::vector<std::string>(argv+1, argv+argc));
  if (args.error) {
    std::cerr << "Error: " << *args.error << "\n";
    return 1;
  }
  assert(args.expander);
  print(lltp::Search(std::move(args.expander)).find_path(args.e[0],args.e[1],display));
}
