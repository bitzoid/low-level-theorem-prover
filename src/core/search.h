#pragma once

#include <vector>
#include <memory>
#include <optional>
#include <algorithm>
#include <iostream>

#include "space.h"

namespace lltp {

  class Search {
    private:
      std::unique_ptr<Expander> expander;

      struct BidiEngine {
        Subexprs forced;
        std::array<Space,2> space;

        BidiEngine(Expr const& e0, Expr const& e1)
          : forced{}
          , space{Space(e0),Space(e1)} {
          forced.add_subexpr(Expr(0));
          forced.add_subexpr(Expr(1));
          for (auto const& v: e0.vars()) {
            forced.add_subexpr(v);
          }
          for (auto const& v: e1.vars()) {
            forced.add_subexpr(v);
          }
        }

        std::optional<Expr> step(Expander const& expander, std::size_t const id, std::function<void(StepInfo const&)> fn = [](StepInfo const&){}) noexcept {
          std::vector<Transition> const& new_found = space[id].step(expander,forced.get(),id,fn);
          for (std::size_t j = 0; j < space.size(); ++j) {
            if (j != id) {
              for (auto const& tr: new_found) {
                if (space[j].find(tr.to)) {
                  return tr.to;
                }
              }
            }
          }
          return std::nullopt;
        }

        bool empty() const noexcept {
          return space[0].empty() && space[1].empty();
        }

        std::optional<Expr> step(Expander const& expander, std::function<void(StepInfo const&)> fn = [](StepInfo const&){}) noexcept {
          for (std::size_t i = 0; i < space.size(); ++i) {
            if (!space[i].empty()) {
              if (auto midway = step(expander, i, fn)) {
                return midway;
              }
            }
          }
          return std::nullopt;
        }

        std::vector<Transition> extract_transitions(Expr const& from, Expr const& midway, Expr const& to) const noexcept {
          auto trs0 = space[0].extract_transitions(from,midway);
          auto trs1 = space[1].extract_transitions(to,midway);
          while (!trs1.empty()) {
            trs0.push_back(trs1.back().reverse());
            trs1.pop_back();
          }
          return trs0;
        }
      };

    public:
      template <typename E = AlgebraExpander<algebra::Field<>>>
      Search() : Search(std::make_unique<E>()) {}
      Search(std::unique_ptr<Expander> ex)
        : expander(std::move(ex)) {}

      std::vector<Transition> find_path(Expr const& from, Expr const& to, std::function<void(StepInfo const&)> fn = [](StepInfo const&) {}) const noexcept {
        BidiEngine engine(from,to);
        if (from == to) {
          return {Transition(from)};
        }
        std::optional<Expr> midway = std::nullopt;
        while (!midway) {
          if (engine.empty()) {
            return {};
          }
          midway = engine.step(*expander, fn);
        }
        auto trs = engine.extract_transitions(from,*midway,to);
        if (trs.size() > 1) {
          for (std::size_t i = 0; i < trs.size();) {
            if (trs[i].ruleTag == RuleTag::Identy) {
              trs.erase(trs.begin()+i);
            } else {
              ++i;
            }
          }
        }
        return trs;
      }
  };
}
