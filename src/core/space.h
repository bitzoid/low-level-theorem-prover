#pragma once

#include <vector>
#include <memory>
#include <optional>
#include <algorithm>
#include <iostream>

#include "adt/bitvecset.h"

#include "expr.h"
#include "rules.h"
#include "strategy.h"

namespace lltp {

  struct StepInfo {
    Expr const& e;
    std::size_t searched;
    std::size_t remain;
    std::size_t children_total;
    std::size_t children_new;
    std::size_t id;
  };

  class Space {
    private:
      strat::BucketsCompressed strategy;

      std::vector<Transition> buf{};
      std::vector<Expr> buf_subs{};
      std::vector<Transition> new_found{};

    public:
      Space(Expr const& expr) : strategy(expr) { }

      std::optional<Transition> find_trans(Expr const& expr) const noexcept {
        return strategy.trans_to(expr);
      }

      bool find(Expr const& expr) const noexcept {
        return strategy.found(expr);
      }

      bool empty() const noexcept {
        return strategy.num_total() == strategy.num_explored();
      }

      std::vector<Transition> const& step(Expander const& expander, std::vector<Expr> const& forced_subs, std::size_t const id, std::function<void(StepInfo const&)> fn = [](StepInfo const&){}) noexcept {
        buf.clear();
        new_found.clear();

        Expr cur = strategy.pop();
        buf_subs = forced_subs;
        cur.subexprs(buf_subs);
        expander.expand_recursive(cur,buf_subs,buf);
        strategy.insert_hint(buf.size());
        for (auto const& tr: buf) {
          if (strategy.insert(tr)) { // insertion happened
            new_found.emplace_back(tr);
          }
        }
        strategy.num_explored();
        fn(StepInfo{cur, strategy.num_explored(), strategy.num_total()-strategy.num_explored(), buf.size(), new_found.size(), id});
        return new_found;
      }

      std::vector<Transition> extract_transitions(Expr const& from, Expr const& to) const noexcept {
        std::vector<Transition> transitions;
        std::optional<Transition> tr = find_trans(to);
        if (!tr) return {};
        while (tr->from != from) {
          transitions.push_back(*tr);
          tr = find_trans(tr->from);
          if (!tr) return {};
        }
        transitions.push_back(*tr);
        std::reverse(transitions.begin(),transitions.end());
        return transitions;
      }
  };
}






