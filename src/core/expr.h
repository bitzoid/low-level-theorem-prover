#pragma once

#include <limits>
#include <type_traits>
#include <vector>
#include <algorithm>
#include <cassert>

#include <iostream>

#include "adt/util.h"
#include "flacl/vector.h"

namespace lltp {

  using ETBase = std::uint16_t;

  enum class EType : ETBase {
    con, var, add, neg, mul, inv
  };

  constexpr inline bool etype_is_leaf(EType type) noexcept {
    switch (type) {
      case EType::con:
      case EType::var:
        return true;
      case EType::add:
      case EType::neg:
      case EType::mul:
      case EType::inv:
      default:
        return false;
    }
  }

  constexpr inline bool etype_is_op(EType type) noexcept {
    switch (type) {
      case EType::add:
      case EType::neg:
      case EType::mul:
      case EType::inv:
        return true;
      case EType::con:
      case EType::var:
      default:
        return false;
    }
  }

  constexpr inline std::size_t etype_num_subs(EType type) noexcept {
    switch (type) {
      case EType::add:
      case EType::mul:
        return 2;
      case EType::neg:
      case EType::inv:
        return 1;
      case EType::con:
      case EType::var:
      default:
        return 0;
    }
  }

  struct NodeId {
    static constexpr std::size_t type_size = 3;
    static constexpr std::size_t data_size = 13;
    ETBase type : type_size;
    ETBase data : data_size; // carries size when operator

    friend bool operator==(NodeId const& l, NodeId const& r) noexcept {
      return l.type == r.type
          && l.data == r.data;
    }

    static EType etype(ETBase type) noexcept {
      return static_cast<EType>(type);
    }
    EType etype() const noexcept {
      return etype(type);
    }

    ETBase size() const noexcept {
      if (etype_is_leaf(etype())) return 1;
      if (etype_is_op(etype())) return data;
      return 0;
    }
  };


  struct NodeIdHash {
    std::size_t operator()(NodeId fid) const noexcept {
      using Tp = ETBase;
      Tp const acc =
        (static_cast<Tp>(fid.type) << 0) ^
        (static_cast<Tp>(fid.data) << NodeId::type_size);
      return std::hash<Tp>()(acc);
    }
  };

  class ExprTrunk {
    friend class Expr;
    private:
      std::vector<NodeId> data;
      template <typename It>
      ExprTrunk(It begin, It end) : data(begin,end) {}
    public:
      ExprTrunk() = delete;
      ExprTrunk(ExprTrunk const&) = default;
      ExprTrunk(ExprTrunk&&) = default;
      ExprTrunk& operator=(ExprTrunk const&) = default;
      ExprTrunk& operator=(ExprTrunk&&) = default;

      friend bool operator==(ExprTrunk const& l, ExprTrunk const& r) noexcept {
        return l.data == r.data;
      }
      friend bool operator!=(ExprTrunk const& l, ExprTrunk const& r) noexcept {
        return !(l == r);
      }
  };

  class Expr {
    public:
      //static constexpr std::size_t max_size = std::numeric_limits<ETBase>::max();
      static constexpr std::size_t max_size = (std::size_t{1ul} << NodeId::data_size)-1;
    private:
      //using Container = std::vector<NodeId>;
      using Container = flacl::Vector<NodeId,16>;
      std::shared_ptr<Container const> tree{};
      std::size_t offset;
      mutable std::size_t hash_value = 0;

      Expr(std::shared_ptr<Container const> tree, std::size_t offset)
        : tree(std::move(tree)), offset(offset) {}

      auto begin() const noexcept {
        return tree->begin() + offset;
      }
      auto end() const noexcept {
        return begin() + begin()->size();
      }

      static Expr join(EType type, Expr const* fe0 = nullptr, Expr const* fe1 = nullptr) noexcept {
        std::size_t const sz = 1 + (fe0?fe0->size():0) + (fe1?fe1->size():0);
        if (sz > max_size) {
          assert(sz <= max_size);
          return Expr('?');
        }
        std::shared_ptr<Container> tree = std::make_shared<Container>();
        tree->reserve(sz);
        tree->push_back(NodeId{static_cast<ETBase>(type),static_cast<ETBase>(sz)});
        if (fe0) {
          for (auto it = fe0->begin(); it < fe0->end(); ++it) {
            tree->push_back(*it);
          }
        }
        if (fe1) {
          for (auto it = fe1->begin(); it < fe1->end(); ++it) {
            tree->push_back(*it);
          }
        }
        return Expr(std::move(tree),0);
      }

      static std::shared_ptr<Container> mono(NodeId fid) noexcept {
        auto p = std::make_shared<Container>();
        p->emplace_back(fid);
        return p;
      }
    public:
      ~Expr() = default;
      Expr() = delete;
      Expr(Expr const& from) = default;
      Expr(Expr&&) = default;
      Expr& operator=(Expr const&) = default;
      Expr& operator=(Expr&&) = default;
      Expr(NodeId fid) noexcept : Expr(mono(fid),0) { }
      explicit Expr(char i) noexcept : Expr(NodeId{static_cast<ETBase>(EType::var),static_cast<ETBase>(i)}) {}
      explicit Expr(int i) noexcept : Expr(NodeId{static_cast<ETBase>(EType::con),static_cast<ETBase>(!!i)}) {
        if (i < 0) {
          *this = -Expr(-i);
        } else if (i > 1) {
          *this = Expr(i-1) + *this;
        }
      }
      explicit Expr(ExprTrunk const& trunk) : Expr(std::make_shared<Container const>(trunk.data.begin(),trunk.data.end()),0) {}

      ExprTrunk trunk() const noexcept {
        return ExprTrunk(begin(),end());
      }

      std::size_t hash() const noexcept {
        if (hash_value == 0) {
          // recompute hash, yes the hash MAY be zero, so what
          hash_value = RangeHash<NodeId,NodeIdHash>()(begin(),end());
        }
        return hash_value;
      }

      static Expr op(EType et, Expr const& lhs, Expr const& rhs) noexcept {
        if (etype_num_subs(et) != 2) {
          assert(etype_num_subs(et) == 2);
          return Expr('?');
        }
        return join(et,&lhs,&rhs);
      }
      static Expr op(EType et, Expr const& sub) noexcept {
        if (etype_num_subs(et) != 1) {
          assert(etype_num_subs(et) == 1);
          return Expr('?');
        }
        return join(et,&sub);
      }

      friend Expr operator+(Expr const& lhs, Expr const& rhs) noexcept {
        return join(EType::add,&lhs,&rhs);
      }
      friend Expr operator-(Expr const& sub) noexcept {
        return join(EType::neg,&sub);
      }
      friend Expr operator*(Expr const& lhs, Expr const& rhs) noexcept {
        return join(EType::mul,&lhs,&rhs);
      }
      friend Expr operator~(Expr const& sub) noexcept {
        return join(EType::inv,&sub);
      }

      std::size_t size() const noexcept {
        return end() - begin();
      }

      std::size_t num() const noexcept {
        return etype_num_subs(type());
      }

      Expr sub(std::size_t i) const noexcept {
        if (i >= num()) {
          assert(i < num());
          return Expr('?');
        }
        ETBase p = 0;
        static constexpr ETBase sub0p = 1;
        if (i == 0) {
          p = sub0p;
        } else if (i == 1) {
          p = sub0p + (begin()+sub0p)->size();
        } else {
          assert(0);
        }
        return Expr(tree,offset+p);
      }

      void subexprs(std::vector<Expr>& subs) const noexcept {
        for (std::size_t i = 0; i < num(); ++i) {
          Expr const e = sub(i);
          bool is_in = false;
          for (auto const& s: subs) {
            is_in = is_in || (s == e);
          }
          if (!is_in) {
            subs.push_back(e);
          }
          e.subexprs(subs);
        }
      }

      EType type() const noexcept {
        return begin()->etype();
      }

      NodeId head() const noexcept {
        return *begin();
      }

      friend bool operator==(Expr const& l, Expr const& r) noexcept {
        return std::equal(l.begin(),l.end(), r.begin(),r.end());
      }

      friend bool operator!=(Expr const& l, Expr const& r) noexcept {
        return !(l == r);
      }

      template <typename Os>
      friend Os& operator<<(Os& os, Expr const& fe) noexcept {
        switch (fe.begin()->etype()) {
          case EType::con: {
            os << static_cast<int>(fe.begin()->data);
          } break;
          case EType::var: {
            os << static_cast<char>(fe.begin()->data);
          } break;
          case EType::add: {
            os << '(' << fe.sub(0) << " + " << fe.sub(1) << ')';
          } break;
          case EType::neg: {
            os << '-' << fe.sub(0);
          } break;
          case EType::mul: {
            os << '(' << fe.sub(0) << " * " << fe.sub(1) << ')';
          } break;
          case EType::inv: {
            os << "1/" << fe.sub(0);
          } break;
          default: {
            os << '?';
          }
        }
        return os;
      }

      Expr replace(std::size_t const i, Expr const& with) const noexcept {
        if (etype_is_leaf(type())) {
          return *this;
        } else if (num() == 1) {
          return join(type(),&with);
        } else {
          if (i == 0) {
            auto const& s = sub(1);
            return join(type(),&with,&s);
          } else {
            auto const& s = sub(0);
            return join(type(),&s,&with);
          }
        }
      }

      void vars(std::vector<Expr>& out) const noexcept {
        if (type() == EType::var) {
          bool contains = false;
          for (auto const& e: out) {
            contains = contains || (*this == e);
          }
          if (!contains) {
            out.push_back(*this);
          }
        } else {
          for (std::size_t i = 0; i < num(); ++i) {
            sub(i).vars(out);
          }
        }
      }

      std::vector<Expr> vars() const noexcept {
        std::vector<Expr> ret;
        vars(ret);
        return ret;
      }
  };

  class Subexprs {
    private:
      std::vector<Expr> subexprs {};

    public:
      inline void add_subexpr(Expr const& expr) noexcept {
        if (std::find(subexprs.begin(),subexprs.end(),expr) == subexprs.end()) {
          subexprs.push_back(expr);
        }
      }

      inline std::vector<Expr> const& get() const noexcept {
        return subexprs;
      }
  };

}

namespace std {
  template<>
  struct hash<lltp::Expr> {
    std::size_t operator()(lltp::Expr const& fe) const noexcept {
      return fe.hash();
    }
  };
}
