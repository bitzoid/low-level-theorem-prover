#include "strategy.h"

#pragma GCC diagnostic ignored "-Wstrict-overflow"
// std::push_heap is mildly broken

namespace lltp {
  namespace strat {
    bool Heap::insert(Transition const& tr) noexcept {
      if (try_insert(tr.to)) { // insertion happened
        queue.emplace_back(tr);
        std::push_heap(queue.begin(),queue.end(),impl::TrCmpG());
        return true;
      }
      return false;
    }

    Expr Heap::pop() noexcept {
      std::pop_heap(queue.begin(),queue.end(),impl::TrCmpG());
      done.emplace_back(queue.back());
      queue.pop_back();
      return done.back().to;
    }
  }
}
