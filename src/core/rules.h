#pragma once

#include <type_traits>
#include <iterator>
#include <string>
#include <iostream>

#include "adt/util.h"

#include "expr.h"

namespace lltp {
  enum class RuleTag {
    AddCom, AddAsc, AddId0, AddInv,
    MulCom, MulAsc, MulId1, MulInv,
    Distrb,
    Identy
  };

  template <EType op>
  struct RuleTagSet {
    //RuleTag com;
    //RuleTag asc;
    //RuleTag idt;
    //RuleTag inv;
    //EType op_inv;
  };
  template <>
  struct RuleTagSet<EType::add> {
    static constexpr RuleTag com = RuleTag::AddCom;
    static constexpr RuleTag asc = RuleTag::AddAsc;
    static constexpr RuleTag idt = RuleTag::AddId0;
    static constexpr RuleTag inv = RuleTag::AddInv;

    static constexpr EType op_inv = EType::neg;
    static constexpr int op_id = 0;
  };
  template <>
  struct RuleTagSet<EType::mul> {
    static constexpr RuleTag com = RuleTag::MulCom;
    static constexpr RuleTag asc = RuleTag::MulAsc;
    static constexpr RuleTag idt = RuleTag::MulId1;
    static constexpr RuleTag inv = RuleTag::MulInv;

    static constexpr EType op_inv = EType::inv;
    static constexpr int op_id = 1;
  };

  inline
  std::string print(RuleTag rt) {
    switch (rt) {
      case RuleTag::AddCom: return "AddCom"; case RuleTag::AddAsc: return "AddAsc"; case RuleTag::AddId0: return "AddId0"; case RuleTag::AddInv: return "AddInv";
      case RuleTag::MulCom: return "MulCom"; case RuleTag::MulAsc: return "MulAsc"; case RuleTag::MulId1: return "MulId1"; case RuleTag::MulInv: return "MulInv";
      case RuleTag::Distrb: return "Distrb";
      case RuleTag::Identy: return "Identy";
      default: return "";
    }
  }

  struct Transition {
    Expr from;
    RuleTag ruleTag;
    Expr to;
    Transition(Expr const& from, RuleTag ruleTag, Expr const& to) : from(from), ruleTag(ruleTag), to(to) {}
    Transition(Expr const& ident) : from(ident), ruleTag(RuleTag::Identy), to(ident) {}
    template <typename Os>
    friend Os& operator<<(Os& os, Transition const& tr) {
      os << "( " << tr.from << " , " << print(tr.ruleTag) << " , " << tr.to << " )";
      return os;
    }
    Transition reverse() const noexcept {
      return Transition(to, ruleTag, from);
    }
  };

  namespace algebra {

    template <EType op>
    class Magma {
      protected:
        using Tags = RuleTagSet<op>;
      public:
        inline void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const noexcept {
          (void)expr;(void)subexprs;(void)transitions;
        }
    };

    template <EType op>
    class Semigroup : protected Magma<op> {
      protected:
        using P = Magma<op>;
      public:
        inline void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const noexcept {
          P::expand(expr,subexprs,transitions);

          if (expr.type() == op) {
            auto const lhs = expr.sub(0);
            auto const rhs = expr.sub(1);
            if (rhs.type() == op) {
              auto const a = lhs;
              auto const b = rhs.sub(0);
              auto const c = rhs.sub(1);
              // (a . (b . c)) -> ((a . b) . c)
              transitions.emplace_back(expr, P::Tags::asc, Expr::op(op,Expr::op(op,a,b),c));
            }
            if (lhs.type() == op) {
              auto const a = lhs.sub(0);
              auto const b = lhs.sub(1);
              auto const c = rhs;
              // ((a . b) . c) -> (a . (b . c))
              transitions.emplace_back(expr, P::Tags::asc, Expr::op(op,a,Expr::op(op,b,c)));
            }
          }
        }
    };

    template <EType op, int cid = RuleTagSet<op>::op_id>
    class Monoid : protected Semigroup<op> {
      protected:
        using P = Semigroup<op>;
        Expr const id = Expr(cid);
      public:
        inline void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const noexcept {
          P::expand(expr,subexprs,transitions);

          transitions.emplace_back(expr, P::Tags::idt, Expr::op(op,expr,id));
          transitions.emplace_back(expr, P::Tags::idt, Expr::op(op,id,expr));

          if (expr.type() == op) {
            auto const lhs = expr.sub(0);
            auto const rhs = expr.sub(1);
            if (lhs == id) {
              transitions.emplace_back(expr, P::Tags::idt, rhs);
            }
            if (rhs == id) {
              transitions.emplace_back(expr, P::Tags::idt, lhs);
            }
          }
        }
    };

    template <EType op, int cid = RuleTagSet<op>::op_id, EType inv = RuleTagSet<op>::op_inv>
    class Group : protected Monoid<op,cid> {
      protected:
        using P = Monoid<op,cid>;
      public:
        inline void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const noexcept {
          P::expand(expr,subexprs,transitions);

          if (expr.type() == op) {
            auto const lhs = expr.sub(0);
            auto const rhs = expr.sub(1);
            if ((rhs.type() == inv && lhs == rhs.sub(0)) || (lhs.type() == inv && rhs == lhs.sub(0))) {
              transitions.emplace_back(expr, P::Tags::inv, P::id);
            }
          } else if (expr == P::id) {
            for (auto const& sub: subexprs) {
              transitions.emplace_back(expr, P::Tags::inv, Expr::op(op,sub,Expr::op(inv,sub)));
              transitions.emplace_back(expr, P::Tags::inv, Expr::op(op,Expr::op(inv,sub),sub));
            }
          }
        }
    };

    template <EType op, int cid = RuleTagSet<op>::op_id, EType inv = RuleTagSet<op>::op_inv>
    class AbelianGroup: protected Group<op,cid,inv> {
      protected:
        using P = Group<op,cid,inv>;
      public:
        inline void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const noexcept {
          P::expand(expr,subexprs,transitions);

          if (expr.type() == op) {
            transitions.emplace_back(expr, P::Tags::com, Expr::op(op,expr.sub(1),expr.sub(0)));
          }
        }
    };

    template <EType mop, EType aop>
    class Distribute {
      public:
        inline void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const noexcept {
          (void)subexprs;
          if (etype_num_subs(expr.type()) != 2) return;
          auto const lhs = expr.sub(0);
          auto const rhs = expr.sub(1);
          if (expr.type() == aop) {
            if ((lhs.type() == mop) && (rhs.type() == mop)) {
              if (lhs.sub(0) == rhs.sub(0)) {
                // (a*b) + (a*c)   ->   a*(b+c)
                transitions.emplace_back(expr, RuleTag::Distrb, Expr::op(mop,lhs.sub(0),Expr::op(aop,lhs.sub(1),rhs.sub(1))));
              }
              if (lhs.sub(1) == rhs.sub(1)) {
                // (a*b) + (c*b)   ->   (a+c)*c
                transitions.emplace_back(expr, RuleTag::Distrb, Expr::op(mop,Expr::op(aop,lhs.sub(0),rhs.sub(0)),lhs.sub(1)));
              }
            }
          } else if (expr.type() == mop) {
            if (rhs.type() == aop) {
              // (a*b) + (a*c)   <-   a*(b+c)
              transitions.emplace_back(expr, RuleTag::Distrb, Expr::op(aop,Expr::op(mop,lhs,rhs.sub(0)),Expr::op(mop,lhs,rhs.sub(1))));
            }
            if (lhs.type() == aop) {
              // (a*b) + (c*b)   <-   (a+c)*c
              transitions.emplace_back(expr, RuleTag::Distrb, Expr::op(aop,Expr::op(mop,lhs.sub(0),rhs),Expr::op(mop,lhs.sub(1),rhs)));
            }
          }
        }
    };

    template <EType mul_op = EType::mul, EType add_op = EType::add,
              int mul_cid = RuleTagSet<mul_op>::op_id, int add_cid = RuleTagSet<add_op>::op_id,
              EType mul_inv = RuleTagSet<mul_op>::op_inv, EType add_inv = RuleTagSet<add_op>::op_inv>
    class Ring {
      protected:
        Monoid<mul_op,mul_cid> multiplication;
        AbelianGroup<add_op,add_cid,add_inv> addition;
        Distribute<mul_op,add_op> distribution;
      public:
        inline void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const noexcept {
          addition.expand(expr,subexprs,transitions);
          multiplication.expand(expr,subexprs,transitions);
          distribution.expand(expr,subexprs,transitions);
        }
    };

    template <EType mul_op = EType::mul, EType add_op = EType::add,
              int mul_cid = RuleTagSet<mul_op>::op_id, int add_cid = RuleTagSet<add_op>::op_id,
              EType mul_inv = RuleTagSet<mul_op>::op_inv, EType add_inv = RuleTagSet<add_op>::op_inv>
    class Field {
      protected:
        AbelianGroup<mul_op,mul_cid,mul_inv> multiplication;
        AbelianGroup<add_op,add_cid,add_inv> addition;
        Distribute<mul_op,add_op> distribution;
        std::vector<Expr> const subexpr_buf = {Expr(mul_cid)};
      public:
        inline void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const noexcept {
          distribution.expand(expr,subexprs,transitions);
          addition.expand(expr,subexprs,transitions);
          multiplication.expand(expr,subexpr_buf,transitions);
        }
    };

  }

  class Expander {
    public:
      virtual ~Expander() {}

      virtual void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const = 0;

      std::vector<Transition> expand(Expr const& expr, std::vector<Expr> const& subexprs) const {
        std::vector<Transition> transitions;
        expand(expr,subexprs,transitions);
        return transitions;
      }

      void expand_recursive(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const noexcept {
        expand(expr,subexprs,transitions);
        if (etype_is_op(expr.type())) {
          for (std::size_t i = 0; i < expr.num(); ++i) {
            std::size_t const top = transitions.size();
            expand_recursive(expr.sub(i),subexprs,transitions);
            for (std::size_t k = top; k < transitions.size(); ++k) {
              transitions[k] = Transition(
                expr.replace(i,transitions[k].from)
              , transitions[k].ruleTag
              , expr.replace(i,transitions[k].to)
              );
            }
          }
        }
      }

      auto expand_recursive(Expr const& expr, std::vector<Expr> const& subexprs) const noexcept{
        std::vector<Transition> transitions;
        expand_recursive(expr,subexprs,transitions);
        return transitions;
      }
  };

  class LegacyFieldExpander : public Expander {
    private:
      Expr const zero = Expr(0);
      Expr const one = Expr(1);
    public:
      virtual void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const override {
        transitions.emplace_back(expr, RuleTag::AddId0, expr + zero);
        transitions.emplace_back(expr, RuleTag::MulId1, expr * one);

        if (expr.type() == EType::add) {
          auto const a = expr.sub(0);
          auto const rhs = expr.sub(1);
          transitions.emplace_back(expr, RuleTag::AddCom, rhs + a);
          if (rhs.type() == EType::add) {
            auto const b = rhs.sub(0);
            auto const c = rhs.sub(1);
            transitions.emplace_back(expr, RuleTag::AddAsc, (a + b) + c);
          } else if (rhs == zero) {
            transitions.emplace_back(expr, RuleTag::AddId0, a);
          } else if (rhs.type() == EType::neg) {
            if (a == rhs.sub(0)) {
              transitions.emplace_back(expr, RuleTag::AddInv, zero);
            }
          }
          // Distr
          if (a.type() == EType::mul) {
            if (rhs.type() == EType::mul) {
              if (a.sub(0) == rhs.sub(0)) {
                transitions.emplace_back(expr, RuleTag::Distrb, a.sub(0) * (a.sub(1) + rhs.sub(1)));
              }
            }
          }
        } else if (expr == zero) {
          for (auto const& sub: subexprs) {
            transitions.emplace_back(expr, RuleTag::AddInv, sub + (-sub));
          }
          //transitions.emplace_back(expr, RuleTag::AddInv, zero + (-zero));
          //transitions.emplace_back(expr, RuleTag::AddInv, one + (-one));
        } else if (expr.type() == EType::mul) {
          auto const a = expr.sub(0);
          auto const rhs = expr.sub(1);
          transitions.emplace_back(expr, RuleTag::MulCom, rhs * a);
          if (rhs.type() == EType::mul) {
            auto const b = rhs.sub(0);
            auto const c = rhs.sub(1);
            transitions.emplace_back(expr, RuleTag::MulAsc, (a * b) * c);
          } else if (rhs == one) {
            transitions.emplace_back(expr, RuleTag::MulId1, a);
          } else if (rhs.type() == EType::inv) {
            if (a == rhs.sub(0)) {
              transitions.emplace_back(expr, RuleTag::MulInv, one);
            }
          }
          // Distr
          if (rhs.type() == EType::add) {
            transitions.emplace_back(expr, RuleTag::Distrb, (a * rhs.sub(0)) + (a * rhs.sub(1)));
          }
        } else if (expr == one) {
          for (auto const& sub: subexprs) {
            if (sub != zero) { // incomplete test
              transitions.emplace_back(expr, RuleTag::MulInv, sub * (~sub));
            }
          }
          //transitions.emplace_back(expr, RuleTag::MulInv, one * (~one));
        }
      }
  };

  template <typename Algebra>
  class AlgebraExpander : public Expander {
    private:
      Algebra theory;
    public:
      virtual void expand(Expr const& expr, std::vector<Expr> const& subexprs, std::vector<Transition>& transitions) const override {
        theory.expand(expr,subexprs,transitions);
      }
  };

}
