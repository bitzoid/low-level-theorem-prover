#pragma once

#include "rules.h"
#include "compression.h"
#include "adt/bitvecset.h"

#include "unordered_set"
#include <vector>
#include <queue>
#include <deque>
#include <map>

#include <iostream>

namespace lltp {

  namespace strat {

    namespace impl {
      template <typename Cmp = std::less<std::size_t>>
      struct TrCmp {
        inline bool operator()(Transition const& lhs, Transition const& rhs) const noexcept {
          //if (Cmp()(lhs.to->depth(),rhs.to->depth())) return true; // makes things slower for some reason
          if (Cmp()(lhs.to.size(),rhs.to.size())) return true;
          return false;
        }
      };

      using TrCmpG = TrCmp<std::greater<std::size_t>>;

      class Discovered {
        private:
          //BitVecSet<std::uint64_t> discovered{};
          std::unordered_set<std::uint64_t> discovered{};
        protected:
          Discovered(Expr const& expr) : discovered({expr.hash()}) {}
          bool found(Expr const& expr) const noexcept {
            return discovered.count(expr.hash()) != 0;
          }
          bool try_insert(Expr const& expr) {
            return discovered.insert(expr.hash()).second;
          }
      };
    }

    class Linear : private impl::Discovered {
      private:
        std::size_t searched = 0;
        std::vector<Transition> queue{};
      public:
        Linear(Expr const& root)
          : Discovered(root)
          , queue({root})
        {}

        std::size_t num_explored() const noexcept {
          return searched;
        }
        std::size_t num_total() const noexcept {
          return queue.size();
        }

        using Discovered::found;

        std::optional<Transition> trans_to(Expr const& expr) const noexcept {
          for (auto const& trans: queue) {
            if (expr == trans.to) {
              return trans;
            }
          }
          return std::nullopt;
        }

        void insert_hint(std::size_t sz) noexcept {
          queue.reserve(queue.size() + sz);
        }

        bool insert(Transition const& tr) noexcept {
          if (try_insert(tr.to)) { // insertion happened
            queue.emplace_back(tr);
            return true;
          }
          return false;
        }

        Expr pop() noexcept {
          Expr result = queue[searched].to;
          ++searched;
          auto best = searched;
          for (std::size_t i = searched+1; i < queue.size(); ++i) {
            if (impl::TrCmp<>()(queue[i],queue[best])) {
              best = i;
            }
          }
          if (best != searched) {
            std::swap(queue[best],queue[searched]);
          }
          return result;
        }
    };

    class Buckets : private impl::Discovered {
      private:
        std::vector<Transition> done{};
        std::vector<std::deque<Transition>> queue{};
        std::size_t total;
      public:
        Buckets(Expr const& root)
          : Discovered(root)
          , queue()
          , total(1) {
          queue.reserve(16);
          queue.resize(root.size()+1);
          queue[root.size()].emplace_back(root);
        }

        std::size_t num_explored() const noexcept {
          return done.size();
        }
        std::size_t num_total() const noexcept {
          return total;
        }

        using Discovered::found;

        std::optional<Transition> trans_to(Expr const& expr) const noexcept {
          for (auto const& trans: done) {
            if (expr == trans.to) {
              return trans;
            }
          }
          for (auto const& v: queue) {
            for (auto const& trans: v) {
              if (expr == trans.to) {
                return trans;
              }
            }
          }
          return std::nullopt;
        }

        void insert_hint(std::size_t) noexcept {
        }

        bool insert(Transition const& tr) noexcept {
          if (try_insert(tr.to)) { // insertion happened
            ++total;
            std::size_t const bucket = tr.to.size();
            if (queue.size() <= bucket) {
              queue.resize(bucket+1);
            }
            queue[bucket].emplace_back(tr);
            return true;
          }
          return false;
        }

        Expr pop() noexcept {
          auto it = queue.begin();
          while (it != queue.end() && it->empty()) ++it;
          if (it == queue.end()) return Expr('?');
          Transition result = it->front();
          it->pop_front();
          done.emplace_back(result);
          return result.to;
        }
    };

    class PriorityQueue : private impl::Discovered, private std::priority_queue<Transition,std::vector<Transition>,impl::TrCmpG> {
      private:
        using Queue = std::priority_queue<Transition,std::vector<Transition>,impl::TrCmpG>;
        std::vector<Transition> done{};
      public:
        PriorityQueue(Expr const& root)
          : Discovered(root)
          , Queue()
          , done() {
          Queue::emplace(root);
        }

        std::size_t num_explored() const noexcept {
          return done.size();
        }
        std::size_t num_total() const noexcept {
          return done.size() + Queue::size();
        }

        using Discovered::found;

        std::optional<Transition> trans_to(Expr const& expr) const noexcept {
          for (auto const& trans: done) {
            if (expr == trans.to) {
              return trans;
            }
          }
          for (auto const& trans: Queue::c) {
            if (expr == trans.to) {
              return trans;
            }
          }
          return std::nullopt;
        }

        void insert_hint(std::size_t sz) noexcept {
          Queue::c.reserve(Queue::c.size() + sz);
        }

        bool insert(Transition const& tr) noexcept {
          if (try_insert(tr.to)) { // insertion happened
            Queue::emplace(tr);
            return true;
          }
          return false;
        }

        Expr pop() noexcept {
          done.emplace_back(Queue::top());
          Queue::pop();
          return done.back().to;
        }
    };

    class Heap : private impl::Discovered {
      private:
        std::vector<Transition> done{};
        std::vector<Transition> queue{};
      public:
        Heap(Expr const& root)
          : Discovered(root)
          , done()
          , queue() {
          queue.emplace_back(root);
        }

        std::size_t num_explored() const noexcept {
          return done.size();
        }
        std::size_t num_total() const noexcept {
          return done.size() + queue.size();
        }

        using Discovered::found;

        std::optional<Transition> trans_to(Expr const& expr) const noexcept {
          for (auto const& trans: done) {
            if (expr == trans.to) {
              return trans;
            }
          }
          for (auto const& trans: queue) {
            if (expr == trans.to) {
              return trans;
            }
          }
          return std::nullopt;
        }

        void insert_hint(std::size_t sz) noexcept {
          queue.reserve(queue.size() + sz);
        }

        bool insert(Transition const& tr) noexcept;

        Expr pop() noexcept;
    };

    class BucketsCompressed : private impl::Discovered {
      private:
        using EC = ExprCompressor<>;
        using T = EC::transition_type;
        EC const ec;
        std::vector<T> done{};
        std::vector<std::deque<T>> queue{};
        std::size_t total;
      public:
        BucketsCompressed(Expr const& root)
          : Discovered(root)
          , ec(CompressTable(root))
          , queue()
          , total(1) {
          queue.reserve(32);
          queue.resize(root.size()+1);
          queue[root.size()].emplace_back(ec.compress(Transition(root)));
        }

        std::size_t footprint() const noexcept {
          std::size_t sum = 0;
          sum += done.capacity()*sizeof(T);
          sum += queue.capacity()*sizeof(std::deque<T>);
          for (auto const& q: queue) {
            sum += q.size()*sizeof(T);
          }
          return sum;
        }

        std::size_t num_explored() const noexcept {
          return done.size();
        }
        std::size_t num_total() const noexcept {
          return total;
        }

        using Discovered::found;

        std::optional<Transition> trans_to(Expr const& expr) const noexcept {
          auto compr = ec.compress(expr);
          for (auto const& v: queue) {
            for (auto const& trans: v) {
              if (compr == trans.to) {
                return ec.decompress(trans);
              }
            }
          }
          for (auto const& trans: done) {
            if (compr == trans.to) {
              return ec.decompress(trans);
            }
          }
          return std::nullopt;
        }

        void insert_hint(std::size_t) noexcept {
        }

        bool insert(Transition const& tr) noexcept {
          if (try_insert(tr.to)) { // insertion happened
            ++total;
            std::size_t const bucket = tr.to.size();
            if (queue.size() <= bucket) {
              queue.resize(bucket+1);
            }
            queue[bucket].emplace_back(ec.compress(tr));
            return true;
          }
          return false;
        }

        Expr pop() noexcept {
          auto it = queue.begin();
          while (it != queue.end() && it->empty()) ++it;
          if (it == queue.end()) return Expr('?');
          auto result = it->front();
          it->pop_front();
          done.emplace_back(result);
          //std::cerr << "footprint: " << (footprint()/1024/1024) << " MB\n";
          return ec.decompress(result.to);
        }
    };

  }
}
