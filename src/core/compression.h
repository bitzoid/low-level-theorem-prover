#pragma once

#include "expr.h"
#include "rules.h"

#include <bitset>
#include <vector>
#include <optional>
#include <variant>
#include <cassert>

namespace lltp {
  class CompressTable {
    public:
      using key_type = char;
      using value_type = std::uint8_t;
      using size_type = std::size_t;
    private:
      std::vector<key_type> table {};
      size_type find(key_type x) const noexcept {
        size_type i = 0;
        while (i < table.size()) {
          if (table[i] == x) break;
          ++i;
        }
        return i;
      }
    public:
      CompressTable() = default;
      CompressTable(Expr const& expr) {
        for (auto const& var: expr.vars()) {
          assert(var.type() == EType::var);
          insert(static_cast<key_type>(var.head().data));
        }
      }

      value_type size() const noexcept {
        return table.size();
      }
      // idempotent
      value_type insert(key_type x) noexcept {
        auto const i = find(x);
        if (i == size()) {
          table.push_back(x);
        }
        return static_cast<value_type>(i);
      }
      // size if x is unknown
      value_type value(key_type x) const noexcept {
        return static_cast<value_type>(find(x));
      }
      // 0 if v is unknown
      key_type key(value_type v) const noexcept {
        if (v < size()) return table[v];
        return key_type{0};
      }
  };

  template <std::size_t N = sizeof(ExprTrunk)*8>
  class ExprCompressor {
    public:
      using compressed_value = std::bitset<N>;
      using value_type = std::variant<compressed_value,ExprTrunk>;
      struct transition_type {
        value_type from;
        RuleTag ruleTag;
        value_type to;
      };
    private:
      CompressTable const ct;
      std::uint64_t const numbits;

      static std::uint64_t calc_numbits(std::uint64_t x) noexcept {
        // yes, this can be done much faster, but this function is literally called ONCE
        if (x == 0) return 0;
        std::uint64_t r = 0;
        for (auto i = x-1; i; i >>= 1) {
          ++r;
        }
        return r;
      }

      // big endian!
      static void write(compressed_value& cv, std::size_t& offset, std::size_t bits, std::uint64_t value) noexcept {
        while (bits--) {
          cv.set(offset, (value >> bits) & 1ull);
          ++offset;
        }
      }
      static std::uint64_t read(compressed_value const& cv, std::size_t& offset, std::size_t bits) noexcept {
        std::uint64_t value = 0;
        while (bits--) {
          value = (value << 1) | cv.test(offset);
          ++offset;
        }
        return value;
      }
      bool compress(compressed_value& cv, std::size_t& offset, Expr const& expr) const noexcept {
        if (offset + NodeId::type_size >= N) return false;
        auto const node = expr.head();
        write(cv, offset, NodeId::type_size, node.type);
        if (etype_is_leaf(node.etype())) {
          if (node.etype() == EType::con) {
            if (offset + 1 >= N) return false;
            write(cv, offset, 1, node.data);
          } else if (node.etype() == EType::var) {
            if (offset + numbits >= N) return false;
            write(cv, offset, numbits, ct.value(node.data));
          } else {
            return false;
          }
        } else if (etype_is_op(node.etype())) {
          for (std::size_t i = 0; i < expr.num(); ++i) {
            if (!compress(cv, offset, expr.sub(i))) return false;
          }
        } else {
          return false;
        }
        return true;
      }
      std::optional<Expr> decompress(compressed_value const& cv, std::size_t& offset) const noexcept {
        if (offset + NodeId::type_size >= N) return std::nullopt;
        auto const etype = NodeId::etype(read(cv, offset, NodeId::type_size));
        if (etype_is_leaf(etype)) {
          if (etype == EType::con) {
            if (offset + 1 >= N) return std::nullopt;
            auto const data = read(cv, offset, 1);
            return Expr(static_cast<int>(data));
          } else if (etype == EType::var) {
            if (offset + numbits >= N) return std::nullopt;
            char const var = ct.key(read(cv, offset, numbits));
            return Expr(var);
          } else {
            return std::nullopt;
          }
        } else if (etype_is_op(etype)) {
          if (etype_num_subs(etype) == 1) {
            std::optional<Expr> const e0 = decompress(cv, offset);
            if (!e0) return std::nullopt;
            return Expr::op(etype,*e0);
          } else if (etype_num_subs(etype) == 2) {
            std::optional<Expr> const e0 = decompress(cv, offset);
            if (!e0) return std::nullopt;
            std::optional<Expr> const e1 = decompress(cv, offset);
            if (!e1) return std::nullopt;
            return Expr::op(etype,*e0,*e1);
          } else {
            return std::nullopt;
          }
        } else {
          return std::nullopt;
        }
      }

      struct Fmt {
        compressed_value cv;
        template <typename Os>
        friend Os& operator<<(Os& os, Fmt const& fmt) {
          os << fmt.cv; // extensibility
          return os;
        }
      };
    public:
      ExprCompressor(CompressTable const& ct) : ct{ct}, numbits{calc_numbits(this->ct.size())} {}
      ExprCompressor(CompressTable&& ct) : ct{std::move(ct)}, numbits{calc_numbits(this->ct.size())} {}

      value_type compress(Expr const& expr) const {
        compressed_value cv;
        std::size_t offset = 0;
        if (compress(cv,offset,expr)) {
          return cv;
        } else {
          return expr.trunk();
        }
      }

      Expr decompress(value_type const& value) const {
        if (std::holds_alternative<compressed_value>(value)) {
          std::size_t offset = 0;
          if (auto expr = decompress(std::get<compressed_value>(value),offset)) {
            return *expr;
          } else {
            return Expr('?');
          }
        } else {
          assert(std::holds_alternative<ExprTrunk>(value));
          return Expr(std::get<ExprTrunk>(value));
        }
      }

      transition_type compress(Transition const& tr) const {
        return transition_type{compress(tr.from),tr.ruleTag,compress(tr.to)};
      }
      Transition decompress(transition_type const& tr) const {
        return Transition(decompress(tr.from),tr.ruleTag,decompress(tr.to));
      }
  };
}
