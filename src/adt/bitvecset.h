#pragma once

#include <array>
#include <vector>

namespace lltp {
  template <typename UInt = std::uint64_t, std::size_t B = 4>
  class BitVecSet {
    private:
      static constexpr std::size_t W = 8*sizeof(UInt);
      static_assert(0 < B);
      static_assert(B <= W);
      static_assert((W % B) == 0);

      static constexpr UInt mask = (((UInt{1} << (B-1))-UInt{1}) << 1) + UInt{1};
      static constexpr std::size_t TSz = (1ul << B);
      static constexpr bool little_endian = false;
      // big endian performs better if the number space isn't fully used
      // little endian is easier to debug

      struct Node {
        std::array<std::size_t,TSz> table = {};
      };

      static std::size_t advance(UInt const val, std::size_t const shift) noexcept {
        if constexpr (little_endian) {
          return (std::size_t{val} >> shift) & mask;
        } else {
          return (std::size_t{val} >> (W-B-shift)) & mask;
        }
      }


      std::vector<Node> nodes = {Node{}};
      template <typename If0>
      bool traverse(UInt val, If0 if0) const noexcept {
        std::size_t i = 0;
        std::size_t shift = 0;
        bool last = false;
        while (!last) {
          std::size_t const sub = advance(val, shift);
          shift += B;
          last = (8*sizeof(UInt)) == shift;
          if (nodes[i].table[sub] == 0) {
            if (!if0(i,sub,last)) {
              return false;
            }
          }
          i = nodes[i].table[sub];
        }
        return true;
      }
    public:
      BitVecSet() {}
      BitVecSet(std::initializer_list<UInt> il) {
        for (auto it = il.begin(); it != il.end(); ++it) {
          insert(*it);
        }
      }

      bool insert(UInt const val) noexcept {
        bool inserted = false;
        traverse(val,[this,&inserted](std::size_t const i, std::size_t const sub, bool const last) {
          if (last) {
            nodes[i].table[sub] = ~std::size_t{0};
          } else {
            nodes[i].table[sub] = nodes.size();
            nodes.push_back(Node{});
          }
          inserted = true;
          return true;
        });
        return inserted;
      }
      bool find(UInt const val) const noexcept {
        return traverse(val,[this](std::size_t, std::size_t, bool) {
          return false;
        });
      }

      std::size_t count(UInt const val) const noexcept {
        return static_cast<std::size_t>(find(val));
      }

      std::size_t memsz() const noexcept {
        return nodes.size() * sizeof(Node);
      }

      template <typename Os>
      friend Os& operator<<(Os& os, BitVecSet const& hs) noexcept {
        os << "BitVecSet {\n";
        for (std::size_t i = 0; i < hs.nodes.size(); ++i) {
          os << "  " << i << ":";
          for (std::size_t const next: hs.nodes[i].table) {
            os << "\t" << next;
          }
          os << "\n";
        }
        os << "}";
        return os;
      }
  };
}

#include <unordered_set>
#include <iostream>
#include <cassert>

namespace lltp {
  namespace test {
    template <typename UInt, std::size_t B>
    inline
    void unit_test_bvset(unsigned const mod, unsigned const num = 10) {
      BitVecSet<UInt,B> hs;
      std::unordered_set<UInt> set;
      {
        UInt a = 53;
        for (std::size_t i = 0; i < 1ul<<num; ++i) {
          bool const inserted = hs.insert(a);
          bool const should = set.insert(a).second;
          (void)inserted;
          (void)should;
          assert(inserted == should);
          assert(hs.find(a));
          a += static_cast<UInt>(i);
          a *= 17;
          a %= (1ul<<mod);
        }
      }
      //std::cout << hs << "\n";
      std::size_t tested = 0;
      for (UInt x = 0; x < static_cast<UInt>(~UInt{0ul}); ++x) {
        if (set.count(x) == 0) {
          ++tested;
          assert(!hs.find(x));
          if (tested >= (1ul<<num)) break;
        } else {
          assert(hs.find(x));
        }
      }
      std::cout << hs.memsz() << " byte\n";
    }

    inline
    void unit_test_bvset() {
      test::unit_test_bvset<std::uint64_t,4>(12);
      test::unit_test_bvset<std::uint16_t,8>(12);
      test::unit_test_bvset<std::uint16_t,4>(12);
      test::unit_test_bvset<std::uint16_t,2>(12);
      test::unit_test_bvset<std::uint16_t,1>(12);
      test::unit_test_bvset<std::uint8_t,8>(6,10);
    }
  }
}
