#pragma once

#include <memory>
#include <functional>
#include <vector>

namespace lltp {
  namespace traits {
    template <typename T> constexpr T& mk_ref();
    template <typename T> constexpr T* mk_ptr();
    template <typename T> constexpr bool has_deref_f(std::decay_t<decltype(*mk_ref<T>())>*) { return true; }
    template <typename T> constexpr bool has_deref_f(...                                  ) { return false; }
    template <typename T> constexpr bool has_deref_v = has_deref_f<T>(nullptr);

    template <typename T>
    struct deref {
      typedef std::decay_t<decltype(*mk_ref<T>())> type;
    };

    template <typename T>
    using deref_t = typename deref<T>::type;
  }

  template <typename T>
  T const& deref_obj(std::shared_ptr<T const> const& p) {
    return *p;
  }

  template <typename T>
  T const& deref_obj(std::unique_ptr<T const> const& p) {
    return *p;
  }

  template <typename T>
  T const& deref_obj(T const* const p) {
    return *p;
  }

  template <typename T>
  T const& deref_obj(T const& p) {
    return p;
  }

  template <typename T>
  T& deref_obj(std::shared_ptr<T> const& p) {
    return *p;
  }

  template <typename T>
  T& deref_obj(std::unique_ptr<T> const& p) {
    return *p;
  }

  template <typename T>
  T& deref_obj(T* const p) {
    return *p;
  }

  template <typename T>
  T& deref_obj(T& p) {
    return p;
  }

  template <typename T, typename Hash = std::hash<T>>
  struct RangeHash {
    std::uint64_t hash_combine(std::uint64_t hash, T const& nv) const noexcept{
      // this magic constant was derived with Wolfram Alpha:
      //   BaseForm[Mod[Floor[2^64 Pi], 2^64], 16]
      static std::uint64_t const magic_constant = 0x243f6a8885a308d3ul;
      // Idea for hash_combine inspired by boost::hash_combine
      return hash ^ (Hash()(nv) + magic_constant + (hash << 24) + (hash >> 40));
    }
    template <typename It>
    std::uint64_t hash_range(It begin, It end, std::uint64_t result = 0) const noexcept {
      for (auto it = begin; it != end; ++it) {
        result = hash_combine(result, *it);
      }
      return result;
    }
    template <typename It>
    std::size_t operator()(It begin, It end) const noexcept {
      return hash_range(begin,end,std::size_t{0});
    }
  };




}
