#pragma once

#include <string>
#include <optional>
#include <variant>
#include <vector>

#include "core/expr.h"

namespace lltp {
  class ExprParser {
    private:
      void skip_white(std::string::const_iterator& begin, std::string::const_iterator end) const noexcept {
        for (; begin < end; ++begin) {
          if (' ' != *begin && '\t' != *begin && '\r' != *begin && '\n' != *begin) break;
        }
      }

      enum class Token {
        open, close, invalid
      };

      static bool is_var(char const c) noexcept {
        return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
      }

      std::variant<int,char,Token,EType> tokenise(std::string::const_iterator& begin, std::string::const_iterator end) const noexcept {
        skip_white(begin,end);
        if (begin >= end) return Token::invalid;
        if (false) {
        } else if (')' == *begin) {
          ++begin;
          return Token::close;
        } else if ('(' == *begin) {
          ++begin;
          return Token::open;
        } else if (is_var(*begin)) {
          return *begin++;
        } else if ('+' == *begin) {
          ++begin;
          return EType::add;
        } else if ('-' == *begin) {
          ++begin;
          return EType::neg;
        } else if ('*' == *begin) {
          ++begin;
          return EType::mul;
        } else if ('~' == *begin) {
          ++begin;
          return EType::inv;
        } else if (('1' == *begin) && (begin+1 < end) && ('/' == *(begin+1))) {
          begin += 2;
          return EType::inv;
        } else {
          bool valid_int = false;
          int i = 0;
          for (; (begin < end) && ('0' <= *begin && *begin <= '9'); ++begin) {
            i = 10*i + (*begin - '0');
            valid_int = true;
          }
          if (valid_int) {
            return i;
          } else {
            return Token::invalid;
          }
        }
      }
      std::optional<Expr> parse_sub(std::string::const_iterator& begin, std::string::const_iterator end) const noexcept {
        skip_white(begin,end);
        if (begin >= end) return std::nullopt;
        auto const tok = tokenise(begin,end);
        if (false) {
        } else if (auto const ptoken = std::get_if<Token>(&tok); ptoken && (*ptoken == Token::close || *ptoken == Token::invalid)) {
          return std::nullopt;
        } else if (auto const ptoken = std::get_if<Token>(&tok); ptoken && (*ptoken == Token::open)) {
          auto const lhs = parse_sub(begin,end);
          if (!lhs) return std::nullopt;
          auto const op = tokenise(begin,end);
          std::optional<EType> type;
          if (false) {
          } else if (auto const pcl = std::get_if<Token>(&op); pcl && (*pcl == Token::close)) {
            return lhs;
          } else if (auto const pop = std::get_if<EType>(&op); pop && (*pop == EType::add)) {
            type = EType::add;
          } else if (auto const pop = std::get_if<EType>(&op); pop && (*pop == EType::mul)) {
            type = EType::mul;
          } else {
            return std::nullopt;
          }
          auto const rhs = parse_sub(begin,end);
          if (!rhs) return std::nullopt;
          auto const close = tokenise(begin,end);
          if (auto const pcl = std::get_if<Token>(&close); pcl && (*pcl == Token::close)) {
            if (type == EType::add) {
              return (*lhs + *rhs);
            } else if (type == EType::mul) {
              return (*lhs * *rhs);
            }
          }
        } else if (auto const pneg = std::get_if<EType>(&tok); pneg && (*pneg == EType::neg)) {
          auto const arg = parse_sub(begin,end);
          if (!arg) return std::nullopt;
          return -*arg;
        } else if (auto const pinv = std::get_if<EType>(&tok); pinv && (*pinv == EType::inv)) {
          auto const arg = parse_sub(begin,end);
          if (!arg) return std::nullopt;
          return ~*arg;
        } else if (auto const pint = std::get_if<int>(&tok)) {
          return Expr(*pint);
        } else if (auto const pchar = std::get_if<char>(&tok)) {
          return Expr(*pchar);
        }
        return std::nullopt;
      }
    public:
      std::optional<Expr> operator()(std::string str) const noexcept {
        str = "(" + str + ")";
        auto begin = str.cbegin();
        auto end = str.cend();
        auto expr = parse_sub(begin, end);
        if (begin == end) {
          return expr;
        } else {
          return std::nullopt;
        }
      }
  };
}
