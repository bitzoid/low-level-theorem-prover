#pragma once

/*
 *  flat-container-lib
 *  Copyright (C) 2020  bitzoid
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* This header-only library provides the following two types:
 *
 *   flacl::Array<T,N>
 *       A contiguous container with fully automatic storage. Is limited to holding N elements.
 *       The interface of Array closely mirrors that of std::vector, but inserting past N elements will not succeed.
 *       Performance: Array has performance comparable to std::array. It uses size_t more bytes.
 *
 *   flacl::Vector<T,[N],[A]>
 *       A contiguous container optimised to use N automatic storage locations and resorts to dynamic memory if more is required. Wraps std::vector.
 *       The container will use the nested buffer of N elements and hence requires no allocations of dynamic memory until that point.
 *       Once more data is inserted the container switches over to the regular behaviour of std::vector. This container is a generalised version.
 *       Note: Once the required data spills over, the internal storage is dead weight.
 *       An instance of Vector<T> (with default N) attemts to exhaust the size of one cache line.
 *       Performance: Vector has some overhead compared to std::array and may deteriorate to the performance of std::vector.
 *
 *
 * Performance benchmark (construction + iteration)
 *        std::set:  187.686us
 *     std::un_set:  209.669us
 *       std::list:  120.314us
 *      std::deque:   90.872us
 *     std::vector:   33.998us
 *   flacl::Vector:   10.945us
 *    flacl::Array:    5.726us
 *      std::array:    5.740us
 *
 * The helper types and constants in namespace detail should not be used from outside this library.
 * The library is encapsulation-safe: It can be wrapped namespaces arbitrarily.
 *
 */

#include <array>
#include <vector>
#include <variant>
#include <algorithm>
#include <type_traits>

namespace flacl {
  template <typename T, std::size_t N>
  class Array {
    public:
      /* array types */
      using value_type              = typename std::array<T,N>::value_type;
      using size_type               = typename std::array<T,N>::size_type;
      using difference_type         = typename std::array<T,N>::difference_type;
      using reference               = typename std::array<T,N>::reference;
      using const_reference         = typename std::array<T,N>::const_reference;
      using pointer                 = typename std::array<T,N>::pointer;
      using const_pointer           = typename std::array<T,N>::const_pointer;
      using iterator                = typename std::array<T,N>::iterator;
      using const_iterator          = typename std::array<T,N>::const_iterator;
      using reverse_iterator        = typename std::array<T,N>::reverse_iterator;
      using const_reverse_iterator  = typename std::array<T,N>::const_reverse_iterator;

    private:
      using U = std::aligned_storage_t<sizeof(T), alignof(T)>;
      U storage[N];
      size_type sz;

      template <typename It>
      using IsLegacyInputIterator = std::enable_if_t<std::is_convertible_v<typename std::iterator_traits<It>::iterator_category,std::input_iterator_tag>>;

      constexpr iterator mkit(size_type i = 0) noexcept {
        return reinterpret_cast<iterator>(storage)+i;
      }
      constexpr const_iterator mkit(size_type i = 0) const noexcept {
        return reinterpret_cast<const_iterator>(storage)+i;
      }
      constexpr iterator remove_const(const_iterator it) noexcept {
        return mkit(static_cast<size_type>(it - cbegin()));
      }

      bool mkgap(const_iterator pos, size_type num) {
        if (sz + num > N) return false;
        if (pos > cend()) return false;
        iterator to = end() + num-1;
        iterator from = to - num;
        while (from >= pos) {
          if (to >= end()) {
            new(to) T(std::move(*from));
          } else {
            *to = std::move(*from);
          }
          --from;
          --to;
        }
        ++from;
        ++to;
        if (end() < to) to = end();
        for (; from < to; ++from) {
          from->~T();
        }
        sz += num;
        return true;
      }

      iterator prepare_insert(const_iterator pos, difference_type diff) {
        if (diff < 0) return end();
        auto const it = remove_const(pos);
        if (diff == 0) return it;
        if (!mkgap(pos,static_cast<size_type>(diff))) return end();
        return it;
      }

      void resize_shrink(size_type nsz) {
        if constexpr (std::is_trivially_destructible_v<T>) {
          sz = std::min(sz,nsz);
        } else {
          while (sz > nsz) {
            pop_back();
          }
        }
      }

    public:
      Array() noexcept : sz(0) {}
      ~Array() {
        clear();
      }

      template <typename It>
      Array(It from, It to) : Array() {
        insert(begin(),from,to);
      }
      Array(std::initializer_list<T> ilist) : Array() {
        insert(begin(),ilist);
      }

      Array(Array const& from) : Array() {
        *this = from;
      }
      Array(Array&& from) : Array() {
        *this = std::move(from);
      }

      Array& operator=(Array const& from) {
        if (&from == this) return *this;
        clear();
        sz = from.sz;
        if constexpr (std::is_trivially_copy_constructible_v<T>) {
          std::copy(from.storage,from.storage+sz,storage);
        } else {
          for (size_type i = 0; i < sz; ++i) {
            new(mkit(i)) T(*from.mkit(i));
          }
        }
        return *this;
      }
      Array& operator=(Array&& from) {
        if (&from == this) return *this;
        clear();
        sz = from.sz;
        if constexpr (std::is_trivially_move_constructible_v<T>) {
          std::copy(from.storage,from.storage+sz,storage);
        } else {
          for (size_type i = 0; i < sz; ++i) {
            new(mkit(i)) T(std::move(*from.mkit(i)));
          }
        }
        from.clear();
        return *this;
      }

      template <typename... Args>
      bool assign(Args&&... args) {
        clear();
        auto it = insert(begin(),std::forward<Args>(args)...);
        return it != end();
      }
      bool assign(std::initializer_list<T> ilist) {
        clear();
        auto it = insert(begin(),ilist);
        return it != end();
      }

      /* element access */
      constexpr reference operator[](size_type i) noexcept {
        return *mkit(i);
      }
      constexpr const_reference operator[](size_type i) const noexcept {
        return *mkit(i);
      }
      constexpr pointer data() noexcept {
        return mkit();
      }
      constexpr const_pointer data() const noexcept {
        return mkit();
      }

      reference at(size_type pos) {
        if (pos >= size()) {
          #ifdef __cpp_exceptions
            throw std::out_of_range("Invalid index for Array");
          #endif
        }
        return operator[](pos);
      }
      const_reference at(size_type pos) const {
        if (pos >= size()) {
          #ifdef __cpp_exceptions
            throw std::out_of_range("Invalid index for Array");
          #endif
        }
        return operator[](pos);
      }
      constexpr reference front() noexcept {
        return *begin();
      }
      constexpr const_reference front() const noexcept {
        return *begin();
      }
      constexpr reference back() noexcept {
        return *std::prev(end());
      }
      constexpr const_reference back() const noexcept {
        return *std::prev(end());
      }

      /* iterators */
      constexpr iterator begin() noexcept { return mkit(); }
      constexpr const_iterator begin() const noexcept { return mkit(); }
      constexpr const_iterator cbegin() const noexcept { return mkit(); }
      iterator end() noexcept { return mkit(sz); }
      const_iterator end() const noexcept { return mkit(sz); }
      const_iterator cend() const noexcept { return mkit(sz); }

      reverse_iterator rbegin() noexcept { return std::make_reverse_iterator(end()); }
      const_reverse_iterator rbegin() const noexcept { return std::make_reverse_iterator(end()); }
      const_reverse_iterator crbegin() const noexcept { return std::make_reverse_iterator(cend()); }
      constexpr reverse_iterator rend() noexcept { return std::make_reverse_iterator(begin()); }
      constexpr const_reverse_iterator rend() const noexcept { return std::make_reverse_iterator(begin()); }
      constexpr const_reverse_iterator crend() const noexcept { return std::make_reverse_iterator(cbegin()); }

      /* capacity */
      constexpr size_type max_size() const noexcept {
        return N;
      }
      constexpr size_type capacity() const noexcept {
        return max_size();
      }
      constexpr bool reserve(size_type nsz) noexcept {
        return nsz <= max_size();
      }
      bool empty() const noexcept {
        return cbegin() == cend();
      }
      size_type size() const noexcept {
        return sz;
      }

      /* modifiers */
      void clear() noexcept {
        resize_shrink(0);
      }
      bool pop_back() noexcept {
        if (empty()) return false;
        mkit(--sz)->~T();
        return true;
      }
      iterator push_back(T const& value) {
        if (size() >= max_size()) return end();
        auto it = mkit(sz++);
        new(it) T(value);
        return it;
      }
      iterator push_back(T&& value) {
        if (size() >= max_size()) return end();
        auto it = mkit(sz++);
        new(it) T(value);
        return it;
      }
      template <typename... Args>
      iterator emplace_back(Args&&... args) {
        if (size() >= max_size()) {
          return end();
        }
        auto const it = mkit(sz++);
        new(it) T(std::forward<Args>(args)...);
        return it;
      }
      bool resize(size_type nsz, const_reference value) {
        if (!reserve(nsz)) return false;
        if (nsz == sz) return true;
        resize_shrink(nsz);
        while (nsz > sz) {
          push_back(value);
        }
        return true;
      }
      bool resize(size_type nsz) {
        return resize(nsz,T());
      }

      void swap(Array& other) {
        if (sz > other.sz) {
          return other.swap(*this);
        }
        // {sz <= other.sz}
        for (size_type i = 0; i < sz; ++i) {
          std::swap((*this)[i],other[i]);
        }
        for (size_type i = sz; i < other.sz; ++i) {
          new(mkit(i)) T(std::move(other[i]));
        }
        size_type const nsz = other.sz;
        other.resize_shrink(sz);
        sz = nsz;
      }
      friend void swap(Array& lhs, Array& rhs) {
        return lhs.swap(rhs);
      }

      template <typename... Args>
      iterator emplace(const_iterator pos, Args&&... args) {
        if (size() >= max_size()) return end();
        if (!mkgap(pos,1)) return end();
        iterator const it = remove_const(pos);
        new(it) T(std::forward<Args>(args)...);
        return it;
      }

      iterator insert(const_iterator pos, T const& value) {
        return emplace(pos,value);
      }

      template <typename InputIt, typename = IsLegacyInputIterator<InputIt> >
      iterator insert(const_iterator pos, InputIt first, InputIt last) {
        if (auto const ins = prepare_insert(pos,last-first); ins != end()) {
          for (auto it = ins; first < last; ) {
            new(it++) T(*first++);
          }
          return ins;
        }
        return end();
      }

      iterator insert(const_iterator pos, size_type count, const_reference value) {
        if (auto const ins = prepare_insert(pos,static_cast<difference_type>(count)); ins != end()) {
          for (auto it = ins; count--; ) {
            new(it++) T(value);
          }
          return ins;
        }
        return end();
      }

      iterator insert(const_iterator pos, std::initializer_list<T> ilist) {
        return insert(pos,ilist.begin(),ilist.end());
      }

      iterator erase(const_iterator pos) {
        return erase(pos,std::next(pos));
      }

      iterator erase(const_iterator first, const_iterator last) {
        if (last > end()) return end();
        if (first > last) return end();
        if (first < begin()) return end();
        if (first == last) return remove_const(last);
        // {first < last} and in range
        difference_type const num = last-first;
        for (auto it = remove_const(first); it+num < end(); ++it) {
          *it = std::move(*(it + num));
        }
        resize_shrink(sz-static_cast<size_type>(num)); // num guaranteed to be <= sz and >= 0
        return remove_const(first);
      }

      /* comparison functions */
      template <size_type M>
      friend bool operator==(Array const& lhs, Array<T,M> const& rhs) {
        return std::equal(lhs.begin(),lhs.end(),rhs.begin(),rhs.end());
      }
      template <size_type M>
      friend bool operator!=(Array const& lhs, Array<T,M> const& rhs) {
        return !(lhs == rhs);
      }
      template <size_type M>
      friend bool operator<(Array const& lhs, Array<T,M> const& rhs) {
        return std::lexicographical_compare(lhs.begin(),lhs.end(),rhs.begin(),rhs.end(),std::less<T>());
      }
      template <size_type M>
      friend bool operator<=(Array const& lhs, Array<T,M> const& rhs) {
        return std::lexicographical_compare(lhs.begin(),lhs.end(),rhs.begin(),rhs.end(),std::less_equal<T>());
      }
      template <size_type M>
      friend bool operator>(Array const& lhs, Array<T,M> const& rhs) {
        return std::lexicographical_compare(lhs.begin(),lhs.end(),rhs.begin(),rhs.end(),std::greater<T>());
      }
      template <size_type M>
      friend bool operator>=(Array const& lhs, Array<T,M> const& rhs) {
        return std::lexicographical_compare(lhs.begin(),lhs.end(),rhs.begin(),rhs.end(),std::greater_equal<T>());
      }

  };

  namespace detail {

    template <typename T, std::size_t N, typename BaseAlloc>
    class Allocator : private BaseAlloc {
      public:
        using allocator           = BaseAlloc;
        using value_type          = typename allocator::value_type;
        using pointer             = typename allocator::pointer;
        using const_pointer       = typename allocator::const_pointer;
        using size_type           = typename allocator::size_type;
        using difference_type     = typename allocator::difference_type;

        //using is_always_equal                        = std::false_type;
        using is_always_equal                        = typename allocator::is_always_equal; // this is a lie, but we ensure that equality is only queried when it is safe
        using propagate_on_container_copy_assignment = std::true_type;
        using propagate_on_container_move_assignment = std::true_type;
        using propagate_on_container_swap            = std::true_type;

        template <typename U>
        struct rebind {
          using other = Allocator<U,N,BaseAlloc>;
        };

      private:
        using S = std::aligned_storage_t<sizeof(T), alignof(T)>;
        S storage[N];
        // XXX this Allocator boldly ASSUMES that you will NEVER allocate a second chunk of size <= N,
        //     so it DOES NOT track whether it already handed out storage!
        //     if this type is used for anything other than an std::vector, it will most likely break!

      public:
        Allocator() = default;
        Allocator(Allocator const& from) = default;
        Allocator(Allocator&&) = default; // ! (Allocator concept needs this)
        Allocator& operator=(Allocator const&) = default;
        Allocator& operator=(Allocator&&) = default; // ! (Allocator concept needs this)
        ~Allocator() = default;

        [[nodiscard]] pointer allocate(size_type n) {
          if (n <= N) {
            return reinterpret_cast<pointer>(storage);
          } else {
            return BaseAlloc::allocate(n);
          }
        }
        void deallocate(pointer p, size_type n) {
          if (n <= N) {
            //auto const begin = reinterpret_cast<pointer>(storage);
            // {begin <= p && p < begin+N }
          } else {
            BaseAlloc::deallocate(p,n);
          }
        }

        // allocators are not interchangeable as they (may) own the handed out buffers, but we protect the allocator from being interchanged
        friend bool operator==(Allocator const& lhs, Allocator const& rhs) {
          //return &lhs == &rhs; // XXX
          return static_cast<allocator const&>(lhs) == static_cast<allocator const&>(rhs);
        }
        friend bool operator!=(Allocator const& lhs, Allocator const& rhs) {
          return !(lhs == rhs);
        }
    };

    namespace impl {
      template <typename T, std::size_t N, typename BaseAlloc>
      class Vector : private std::vector<T,Allocator<T,N,BaseAlloc>> {
        private:
          using A = Allocator<T,N,BaseAlloc>;
          using V = std::vector<T,A>;
        public:
          using value_type              = typename V::value_type;
          using size_type               = typename V::size_type;
          using difference_type         = typename V::difference_type;
          using allocator_type          = typename V::allocator_type;
          using reference               = typename V::reference;
          using const_reference         = typename V::const_reference;
          using pointer                 = typename V::pointer;
          using const_pointer           = typename V::const_pointer;
          using iterator                = typename V::iterator;
          using const_iterator          = typename V::const_iterator;
          using reverse_iterator        = typename V::reverse_iterator;
          using const_reverse_iterator  = typename V::const_reverse_iterator;

        public:
          Vector() : V() {
            // this is important because we don't want to deal with moving stuff around inside the allocator's array
            reserve(N);
          }
          Vector(Vector const& from) : Vector() {
            *this = from;
          }
          Vector(Vector&& from) : Vector() {
            *this = std::move(from);
          }
          Vector& operator=(Vector const& from) {
            V::operator=(from);
            flatten(*this);
            return *this;
          }
          Vector& operator=(Vector&& from) {
            if (from.size() > this->flat_size()) {
              if (this->is_flat()) {
                // Force unflatten to force this std::vector to throw away its flat buffer.
                // This is bad, because the new buffer will be thrown away immediately.
                // Unfortunately shrink_to_fit is a mere non-binding suggestion.
                reserve(from.size());
              }
              V::operator=(std::move(from));
              // with the normal std::allocator, from will now be empty and flat, otherwise:
              from.clear();
              flatten(from);
            } else {
              clear();
              reserve(from.size());
              for (std::size_t i = 0; i < from.size(); ++i) {
                emplace_back(std::move(from[i]));
              }
              from.clear();
            }
            return *this;
          }
          template <typename It>
          Vector(It from, It to) : Vector() {
            assign(from,to);
          }
          Vector(std::initializer_list<T> ilist) : Vector() {
            assign(ilist);
          }
          using V::assign;

          /* element access */
          using V::operator[];
          using V::data;
          using V::at;
          using V::front;
          using V::back;

          /* iterators */
          using V::begin;
          using V::cbegin;
          using V::end;
          using V::cend;
          using V::rbegin;
          using V::crbegin;
          using V::rend;
          using V::crend;

          /* capacity */
          constexpr size_type flat_size() const noexcept {
            return N;
          }
          bool is_flat() const noexcept {
            return capacity() <= flat_size();
          }
          using V::capacity;
          using V::reserve;
          using V::empty;
          using V::size;

          /* modifiers */
          using V::clear;
          using V::pop_back;
          using V::push_back;
          using V::emplace_back;
          using V::resize;
          using V::emplace;
          using V::insert;
          using V::erase;

          void swap(Vector& other) {
            if (!is_flat() && !other.is_flat()) {
              V::swap(other);
            } else {
              reserve(other.size());
              other.reserve(size());
              // { is_flat() == other.is_flat()) }
              auto& small = (size() < other.size())?*this:other;
              auto& large = (size() < other.size())?other:*this;
              auto const sz = small.size();
              for (std::size_t i = 0; i < sz; ++i) {
                std::swap((*this)[i],other[i]);
              }
              for (std::size_t i = sz; i < large.size(); ++i) {
                small.emplace_back(std::move(large[i]));
              }
              large.resize(sz);
            }
          }
          friend void swap(Vector& lhs, Vector& rhs) {
            return lhs.swap(rhs);
          }

          /* comparison functions */
          template <size_type M>
          friend bool operator==(Vector const& lhs, Vector<T,M,BaseAlloc> const& rhs) {
            return std::equal(lhs.begin(),lhs.end(),rhs.begin(),rhs.end());
          }
          template <size_type M>
          friend bool operator!=(Vector const& lhs, Vector<T,M,BaseAlloc> const& rhs) {
            return !(lhs == rhs);
          }
          template <size_type M>
          friend bool operator<(Vector const& lhs, Vector<T,M,BaseAlloc> const& rhs) {
            return std::lexicographical_compare(lhs.begin(),lhs.end(),rhs.begin(),rhs.end(),std::less<T>());
          }
          template <size_type M>
          friend bool operator<=(Vector const& lhs, Vector<T,M,BaseAlloc> const& rhs) {
            return std::lexicographical_compare(lhs.begin(),lhs.end(),rhs.begin(),rhs.end(),std::less_equal<T>());
          }
          template <size_type M>
          friend bool operator>(Vector const& lhs, Vector<T,M,BaseAlloc> const& rhs) {
            return std::lexicographical_compare(lhs.begin(),lhs.end(),rhs.begin(),rhs.end(),std::greater<T>());
          }
          template <size_type M>
          friend bool operator>=(Vector const& lhs, Vector<T,M,BaseAlloc> const& rhs) {
            return std::lexicographical_compare(lhs.begin(),lhs.end(),rhs.begin(),rhs.end(),std::greater_equal<T>());
          }

          static bool flatten(Vector& vec) {
            if (vec.is_flat()) return true;
            if (vec.size() > vec.flat_size()) return false;
            V buf = std::move(vec); // !

            vec.~Vector();
            new(&vec) Vector();

            vec.reserve(buf.size()); // should be a no-op
            for (size_type i = 0; i < buf.size(); ++i) {
              vec.emplace_back(std::move(buf[i]));
            }
            return vec.is_flat();
          }
      };
    }

    template <typename T, std::size_t N, typename BaseAlloc, typename = void>
    struct Vector_selector {
      using type = impl::Vector<T,N,BaseAlloc>;
    };

    template <typename T, typename BaseAlloc>
    struct Vector_selector<T,0,BaseAlloc,std::enable_if_t<!std::is_same_v<void,BaseAlloc>>> {
      using type = std::vector<T,BaseAlloc>;
    };

    template <typename T, std::size_t N>
    struct Vector_selector<T,N,void> {
      using type = Array<T,N>;
    };

    template <typename T, std::size_t N, typename BaseAlloc>
    using Vector = typename Vector_selector<T,N,BaseAlloc>::type;

    template <typename T, T X, T... Ys>
    inline constexpr T last_arg = last_arg<T,Ys...>;
    template <typename T, T X>
    inline constexpr T last_arg<T,X> = X;

    inline constexpr std::size_t OptVec_CacheLine = last_arg<std::size_t,64
    #ifdef __cpp_lib_hardware_interference_size
      #if __cpp_lib_hardware_interference_size >= 201603
        ,std::hardware_constructive_interference_size
      #endif
    #endif
    >;

    template <typename T, typename BaseAlloc, std::size_t C>
    struct OptVecN_impl {
      template <std::size_t N>
      static constexpr std::size_t find(std::size_t n) {
        if constexpr (sizeof(Vector<T,N,BaseAlloc>) <= C) {
          return find<N+1>(N);
        } else {
          return n;
        }
      }
      static constexpr std::size_t value = find<1>(0);
    };

    template <typename T, std::size_t N, typename BaseAlloc>
    inline constexpr std::size_t OptVecN = N;

    template <typename T, typename BaseAlloc>
    inline constexpr std::size_t OptVecN<T,std::size_t(-1),BaseAlloc> = OptVecN_impl<T,BaseAlloc,OptVec_CacheLine>::value;
  }

  template <typename T, std::size_t N = std::size_t(-1), typename BaseAlloc = std::allocator<T>>
  using Vector = detail::Vector<T,detail::OptVecN<T,N,BaseAlloc>,BaseAlloc>;

  template <typename Vec>
  inline auto flatten(Vec& vec) {
    return Vec::flatten(vec);
  }
}

