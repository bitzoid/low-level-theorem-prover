# lltp -- low level theorem prover

This is a small toybox that allows to prove fundamental algebraic equations from basic axioms. For example, it gives a chain of axioms for why `0*a == 0` in a field.

It is currently not able to prove very complicated expressions and struggles with more than one variable, if only for lack of main memory.

Note also that lltp is **not** capable of providing negative proof. Eg. `1 == 0` will just fail to terminate (until the system crash for lack of memory).

This program is not intended for production, it is purely an experiment on what can be automatically derived from first axioms. There are extreme optimisations possible if some of these limitations are lifted.

## How to run it?

After pulling the repository build the binary by saying `make` in the main directory. You will need a decently recent `g++` with full support for C++17. You can tweak the build mode by selecting one of `MODE:=fast`, `MODE:=hard`, `MODE:=opti` in `makefile.config`. `fast` will speed up the build process, `hard` will harden the binary, good for debugging, and `opti` will create the most efficient binary by far.

The command `./lltp 'a*0' '0'` will take about a second and produce the following proof:

    (a * 0)
    ->	AddId0	((a * 0) + 0)
    ->	AddInv	((a * 0) + (a + -a))
    ->	AddAsc	(((a * 0) + a) + -a)
    ->	AddCom	(-a + ((a * 0) + a))
    ->	AddCom	(-a + (a + (a * 0)))
    ->	MulId1	(-a + ((a * 1) + (a * 0)))
    ->	Distrb	(-a + (a * (1 + 0)))
    ->	AddId0	(-a + (a * 1))
    ->	MulId1	(-a + a)
    ->	AddCom	(a + -a)
    ->	AddInv	0

One more example, showing that `-1 * -1 == 1` takes about ten seconds, producing the following proof:

    (-1 * -1)
    ->	AddId0	((-1 * -1) + 0)
    ->	AddInv	((-1 * -1) + (1 + -1))
    ->	AddCom	((-1 * -1) + (-1 + 1))
    ->	AddAsc	(((-1 * -1) + -1) + 1)
    ->	MulId1	(((-1 * -1) + (-1 * 1)) + 1)
    ->	Distrb	((-1 * (-1 + 1)) + 1)
    ->	AddCom	((-1 * (1 + -1)) + 1)
    ->	AddInv	((-1 * 0) + 1)
    ->	MulCom	((0 * -1) + 1)
    ->	AddId0	(((0 * -1) + 0) + 1)
    ->	MulId1	(((0 * -1) + (0 * 1)) + 1)
    ->	Distrb	((0 * (-1 + 1)) + 1)
    ->	AddCom	((0 * (1 + -1)) + 1)
    ->	AddInv	((0 * 0) + 1)
    ->	AddId0	(((0 * 0) + 0) + 1)
    ->	AddCom	((0 + (0 * 0)) + 1)
    ->	MulId1	(((0 * 1) + (0 * 0)) + 1)
    ->	Distrb	((0 * (1 + 0)) + 1)
    ->	AddId0	((0 * 1) + 1)
    ->	MulId1	(0 + 1)
    ->	AddCom	(1 + 0)
    ->	AddId0	1

## Supported Theories

Currently lltp supports the following theories:

* magma (does nothing)
* semigroup
* monoid
* group
* abelian (or `abelian_group`)
* ring
* field (reimplemented in terms of the above
* `legacy_field`

Currently all of moinoid and up will will yield an infinite search space and thus, never terminate on a false query.
The legacy field implementation can be faster sometimes, but *may* produce false proofs as it may produce a division by zero.

Invokation:

    ./lltp semigroup 2+1 1+2

Will try to find evidence that the equation `((1 + 1) + 1) == (1 + (1 + 1))` holds in a semi-group, which holds because of associativity.

## Future work

It may be useful to create a knowledge base for derived equations such that more complicated things can be derived.

## License

> low-level-theorem-prover
> Copyright (C) 2020  bitzoid
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Author

*  bitzoid {zoid at riseup dot net}
